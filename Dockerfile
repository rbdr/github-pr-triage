FROM python:2-alpine

RUN apk add build-base
RUN apk add libffi-dev
RUN apk add openssl-dev

WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt
COPY . /app

ENTRYPOINT [ "python", "app.py" ])
